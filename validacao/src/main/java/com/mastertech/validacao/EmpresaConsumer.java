package com.mastertech.validacao;

import com.mastertech.clients.Receita;
import com.mastertech.clients.ReceitaClient;
import com.mastertech.empresa.model.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmpresaConsumer {

    @Autowired
    private ReceitaClient receitaClient;

    @KafkaListener(topics = "spec3-eduardo-pereira-2", groupId = "Eduardo-1")
    public void receber(@Payload Empresa empresa) {
        Receita receitaObjeto;
        System.out.println("Recebi a empresa: " + empresa.getNome() + " - CNPJ: " + empresa.getCnpj());
        receitaObjeto = receitaClient.retornaEmpresa(empresa.getCnpj());
        System.out.println("Recebi a empresa: " + receitaObjeto.getNome() + " - CNPJ: " + receitaObjeto.getCapitalSocial());
    }

}
