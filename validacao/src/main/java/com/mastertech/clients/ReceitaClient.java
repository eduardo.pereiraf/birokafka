package com.mastertech.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "receitaClient", url = "https://www.receitaws.com.br/")
public interface ReceitaClient {

    @GetMapping("/v1/cnpj/{cnpj}}")
    Receita retornaEmpresa(@PathVariable String cnpj);

}
