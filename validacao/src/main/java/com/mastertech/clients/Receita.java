package com.mastertech.clients;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Receita {

    private String nome;

    @JsonProperty("capital_social")
    private Double capitalSocial;

    public String getNome() {
        return nome;
    }

    public Double getCapitalSocial() {
        return capitalSocial;
    }

    public void setCapitalSocial(Double capitalSocial) {
        this.capitalSocial = capitalSocial;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


}
