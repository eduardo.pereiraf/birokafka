package com.mastertech.empresa.service;

import com.mastertech.empresa.model.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    public String cadastraEmpresa(Empresa empresa){
        producer.send("spec3-eduardo-pereira-2", empresa);
        return "Cadastro enviado com sucesso";
    }
}
