package com.mastertech.empresa.controller;

import com.mastertech.empresa.model.Empresa;
import com.mastertech.empresa.service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {

    @Autowired
    private EmpresaService empresaService;

    @PostMapping
    public String cadastraEmpresa(@RequestBody Empresa empresa){
        return empresaService.cadastraEmpresa(empresa);
    }
}
