package com.mastertech.processa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProcessaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProcessaApplication.class, args);
	}

}
